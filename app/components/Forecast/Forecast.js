"use strict";

import React from "react";
import {GridList, GridTile} from 'material-ui/GridList'

const styles = {
    gridList: {
        display: 'flex',
        flexWrap: 'nowrap',
        overflowX: 'auto',
    },
    tileStyle: {
        minWidth: 170
    },
    pStyle: {
        margin: 16
    }
};

const Forecast = (props) => {
    let {forecast} = props;

    if (!forecast) return <div></div>;

    return (
        <div>
            <GridList
                style={styles.gridList}
            >
                {forecast.map((dayForecast, i) => {
                    let locale = "en-GB";
                    let options = {weekday: "long", month: "2-digit", day: "2-digit"};
                    return (
                        <GridTile
                            key={i}
                            title={dayForecast.date.toLocaleString(locale, options)}
                            titleBackground="linear-gradient(to top, rgba(0,0,0,0.7) 0%,rgba(0,0,0,0.3) 70%,rgba(0,0,0,0) 100%)"
                            style={styles.tileStyle}
                        >
                            <img src={dayForecast.img}/>
                            <p style={styles.pStyle}>Min: {dayForecast.min} °C</p>
                            <p style={styles.pStyle}>Max: {dayForecast.max} °C</p>
                        </GridTile>
                    );
                })}
            </GridList>
        </div>
    );
};

export default Forecast;