"use strict";

import React from "react";
import {List, ListItem} from 'material-ui/List';
import Avatar from 'material-ui/Avatar';

const Restaurants = (props) => {
    let {restaurants} = props;

    if (!restaurants) return <div></div>;

    return (
        <div>
            <List>
                {restaurants.map((restaurant, i) => {
                    return (
                        <ListItem
                            key={i}
                            leftAvatar={<Avatar src={restaurant.icon}/>}
                            primaryText={restaurant.name}
                            secondaryText={restaurant.vicinity}
                        />
                    );
                })}
            </List>
        </div>
    );
};

export default Restaurants;