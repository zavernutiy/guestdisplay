"use strict";

import React from "react";
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Forecast from "../Forecast/Forecast";

const ForecastDialog = (props) => {

    const actions = [
        <FlatButton
            label="OK"
            primary={true}
            onClick={props.onClose}
        />,
    ];

    return (
        <div>
            <Dialog
                title="Forecast"
                actions={actions}
                modal
                open={props.open}
                onRequestClose={props.onClose}
            >
                <Forecast forecast={props.forecast}/>
            </Dialog>
        </div>
    )
};

export default ForecastDialog;