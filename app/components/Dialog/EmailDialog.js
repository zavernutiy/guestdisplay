"use strict";

import React from "react";
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';


const EmailDialog = (props) => {

    const actions = [
        <FlatButton
            label="Send"
            primary={true}
            onClick={props.onSend}
        />,
        <FlatButton
            label="Cancel"
            primary={true}
            onClick={props.onClose}
        />,
    ];

    return (
        <div>
            <Dialog
                title="Email"
                actions={actions}
                modal
                open={props.open}
                onRequestClose={props.onClose}
            >
                <div>
                    Enter email below
                </div>

                <TextField
                    hintText="example@gmail.com"
                    value={props.email}
                    onChange={props.onChange}
                    fullWidth
                    errorText={props.error}
                />
            </Dialog>
        </div>
    )
};

export default EmailDialog;