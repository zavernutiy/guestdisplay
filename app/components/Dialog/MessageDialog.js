"use strict";

import React from "react";
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';


const MessageDialog = (props) => {

    const actions = [
        <FlatButton
            label="OK"
            primary={true}
            onClick={props.onClose}
        />,
    ];

    return (
        <div>
            <Dialog
                title={props.title}
                actions={actions}
                modal
                open={props.open}
                onRequestClose={props.onClose}
            >
                {props.message}
            </Dialog>
        </div>
    )
};

export default MessageDialog;