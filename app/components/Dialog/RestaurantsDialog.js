"use strict";

import React from "react";
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Restaurants from "../Restaurants/Restaurants";

const RestaurantsDialog = (props) => {

    const actions = [
        <FlatButton
            label="OK"
            primary={true}
            onClick={props.onClose}
        />,
    ];

    return (
        <div>
            <Dialog
                title="Nearby restaurants"
                actions={actions}
                modal
                open={props.open}
                onRequestClose={props.onClose}
                autoScrollBodyContent={true}
            >
                <Restaurants restaurants={props.restaurants}/>
            </Dialog>
        </div>
    )
};

export default RestaurantsDialog;