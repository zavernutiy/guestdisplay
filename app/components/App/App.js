import React, {Component} from "react";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {AppContainer} from 'react-hot-loader'
import RaisedButton from 'material-ui/RaisedButton';
import EmailDialog from "../Dialog/EmailDialog";
import MessageDialog from "../Dialog/MessageDialog";
import RestaurantDialog from "../Dialog/RestaurantsDialog";
import ForecastDialog from "../Dialog/ForecastDialog";

import axios from "axios";
import emailValidator from "email-validator";

import styles from "./App.scss";

/**
 * Hardcoded coordinates
 */
const LAT = -33.867;
const LNG = 151.195;

/**
 * Constants used in nearby places
 */
const COORDINATES = {lat: LAT, lng: LNG};
const RADIUS = 500;

/**
 * Constants used for weather
 */
const WEATHER_API_KEY = "214ce99b6f1cbeea9f6a6dd6ac886d29";
const UNITS = "metric";
const WEATHER_URL = `http://api.openweathermap.org/data/2.5/forecast?lat=${LAT}&lon=${LNG}&appid=${WEATHER_API_KEY}&units=${UNITS}`;

/**
 * Constants used for email (dummy API address)
 */
const EMAIL_URL = "http://echo.jsontest.com/email/test/content/email";

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            restaurants: [],
            openEmailDialog: false,
            email: "",
            emailError: "",
            openMessageDialog: false,
            messageDialogTitle: "",
            messageDialogMessage: "",
            openRestaurantDialog: false,
            dailyForecast: [],
            openForecastDialog: false,
        }
    }

    componentDidMount() {
        this.getPlaces();

        this.getWeather();
    }

    getPlaces = () => {
        let service = new google.maps.places.PlacesService(document.getElementById("map_container"));

        let request = {
            location: COORDINATES,
            radius: RADIUS,
            type: ["restaurant"]
        };

        service.nearbySearch(request, this.placesCallback);
    };

    placesCallback = (results, status) => {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
            this.setState({
                restaurants: results
            });
        }
    };

    getWeather = async() => {
        let errorTitle = "Woops";
        let errorMessage = "Unfortunately we couldn't get weather forecast for you. Please, try again later";

        try {
            let response = await axios.get(WEATHER_URL);
            if (response && response.data && response.data.list) {
                let forecasts = response.data.list;
                let hour;
                let dailyForecast = [];
                forecasts.forEach(forecast => {
                    let date = new Date(forecast.dt * 1000);
                    if (!hour) {
                        hour = date.getUTCHours();
                        dailyForecast.push({
                            min: forecast.main.temp_min,
                            max: forecast.main.temp_max,
                            date,
                            img: `http://openweathermap.org/img/w/${forecast.weather[0].icon}.png`
                        });
                    } else {
                        let currentHour = date.getUTCHours();
                        if (hour === currentHour) {
                            dailyForecast.push({
                                min: forecast.main.temp_min,
                                max: forecast.main.temp_max,
                                date,
                                img: `http://openweathermap.org/img/w/${forecast.weather[0].icon}.png`
                            });
                        }
                    }
                });

                this.setState({dailyForecast});
            } else {
                this.openMessageDialog(errorTitle, errorMessage);
            }
        } catch (err) {
            console.error("Error, could not get weather forecast");
            console.error(err);
            this.openMessageDialog(errorTitle, errorMessage);
        }
    };


    /**
     * Methods linked to email dialogs
     */
    openEmailDialog = () => {
        this.setState({
            openEmailDialog: true
        });
    };

    onCloseEmail = () => {
        this.setState({
            openEmailDialog: false,
            email: "",
            emailError: "",
        });
    };

    onChangeEmail = (event, email) => {
        this.setState({email});
    };

    onSendEmail = async() => {
        let {email} = this.state;

        if (!emailValidator.validate(email)) {
            this.setState({
                emailError: "Email is invalid"
            });

            return;
        }

        try {
            let response = await axios.post(EMAIL_URL, {email});
            this.openMessageDialog("Success", "Email with coupon for hotel SPA was sent successfully");
        } catch (err) {
            console.error("Error, could not send email");
            console.error(err);
            this.openMessageDialog("Woops", "Unfortunately we cannot send you coupon at the moment. Please try again later");
        }

        this.onCloseEmail();
    };


    /**
     * Methods linked to email message dialog
     */
    openMessageDialog = (title, message) => {
        this.setState({
            openMessageDialog: true,
            messageDialogTitle: title,
            messageDialogMessage: message
        });
    };

    onCloseEmailMessageDialog = () => {
        this.setState({
            openMessageDialog: false,
            messageDialogTitle: "",
            messageDialogMessage: ""
        });
    };


    /**
     * Methods linked to Restaurant dialog
     */
    openRestaurantDialog = () => {
        this.setState({
            openRestaurantDialog: true
        });
    };

    onCloseRestaurantDialog = () => {
        this.setState({
            openRestaurantDialog: false
        });
    };


    /**
     * Methods linked to Forecast dialog
     */
    openForecastDialog = () => {
        this.setState({
            openForecastDialog: true
        });
    };

    onCloseForecastDialog = () => {
        this.setState({
            openForecastDialog: false
        });
    };

    render() {
        let {
            openEmailDialog, email, emailError,
            openMessageDialog, messageDialogTitle,
            messageDialogMessage, restaurants,
            openRestaurantDialog, openForecastDialog,
            dailyForecast
        } = this.state;

        return (
            <AppContainer>
                <MuiThemeProvider>
                    <div className={styles.container}>
                        <div className={styles.buttonDiv}>
                            <RaisedButton label="Restaurants in the area" primary fullWidth onClick={this.openRestaurantDialog}/>
                        </div>
                        <div className={styles.buttonDiv}>
                            <RaisedButton label="Weather" primary fullWidth onClick={this.openForecastDialog}/>
                        </div>
                        <div className={styles.buttonDiv}>
                            <RaisedButton label="Get coupon for hotel SPA" primary fullWidth onClick={this.openEmailDialog}/>
                        </div>

                        {/* Empty div as a map container. Cannot initialize PlacesService without it */}
                        <div id="map_container">

                        </div>

                        <EmailDialog
                            open={openEmailDialog}
                            email={email}
                            error={emailError}
                            onSend={this.onSendEmail}
                            onClose={this.onCloseEmail}
                            onChange={this.onChangeEmail}
                        />

                        <MessageDialog
                            open={openMessageDialog}
                            title={messageDialogTitle}
                            message={messageDialogMessage}
                            onClose={this.onCloseEmailMessageDialog}
                        />

                        <RestaurantDialog
                            open={openRestaurantDialog}
                            onClose={this.onCloseRestaurantDialog}
                            restaurants={restaurants}
                        />

                        <ForecastDialog
                            open={openForecastDialog}
                            onClose={this.onCloseForecastDialog}
                            forecast={dailyForecast}
                        />
                    </div>
                </MuiThemeProvider>
            </AppContainer>
        );
    }
}

export default App;