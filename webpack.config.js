var path = require('path');
var webpack = require('webpack');

module.exports = {
    entry: ['babel-polyfill', 'react-hot-loader/patch', path.resolve(__dirname, "app", "app.js")],
    output: {
        filename: 'bundle.js',
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loaders: ['babel-loader'],
            },
            {
                test: /\.scss$/,
                loaders: [
                    'style-loader',
                    'css-loader?modules&importLoaders=1' +
                    '&localIdentName=[name]__[local]___[hash:base64:5]!sass-loader?includePaths[]=' +
                    encodeURIComponent(path.resolve(__dirname, '/app'))
                ]
            },
            {
                test: /\.(jpg|png|svg)$/,
                loader: 'url-loader',
                options: {
                    limit: 25000,
                },
            },
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
    ],
    devServer: {
        historyApiFallback: {
            disableDotRule: true
        },
        hot: true,
        inline: true,
        port: 5353
    }
};